#  Copyright 2019 Queensland Cyber Infrastructure Foundation
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

Namespaces:
  =: au.org.nectar.qriscloud
  std: io.murano
  res: io.murano.resources
  sys: io.murano.system
  conf: io.murano.configuration

Name: Bioconda

Extends: std:Application

Properties:
  instance:
    Contract: $.class(res:Instance).notNull()
  backups:
    Contract: $.class(DuplyBackups)

Methods:
  initialize:
    Body:
      - $._environment: $.find(std:Environment).require()
      - $._debug: true
      - $._linuxLib: null
      - $._duplyLib: null
      - $._bc_user: null

  deploy:
    Body:
      - If: not $.getAttr(deployed,false)
        Then:
          - $._environment.reporter.report($this,'Creating VM for Bioconda.')
          - $securityGroupIngress:
            - ToPort: 22
              FromPort: 22
              IpProtocol: tcp
              External: true
          - $._environment.securityGroupManager.addGroupIngress($securityGroupIngress)
          - $.instance.deploy()
          - $._linuxLib: new(LinuxUtilLib,$,instance => $.instance)
          - $distId: $._linuxLib.distId()
          - If: $distId = 'ubuntu'
            Then:
              - $._bc_user: 'ubuntu'
            Else:
              - $._bc_user: 'ec2-user'
          - $._environment.reporter.report($this,'Bioconda instance is deployed.')
          - $.setAttr(deployed,true)
          - $host: $.instance.ipAddresses[0]
          - $.configure($host)
          - $._environment.reporter.report($this,format('Bioconda is available at {hn}',hn=>$host))

  configure:
    Arguments:
      - host:
          Contract: $.string().notNull()
    Body:
      - $.deploy()
      - $._environment.reporter.report($this,'Configuring Bioconda.')
      - $script: |
          #!/bin/bash
          wget -nv -P /tmp https://repo.continuum.io/miniconda/Miniconda2-latest-Linux-x86_64.sh
          chmod +x /tmp/Miniconda2-latest-Linux-x86_64.sh
          sudo su -s /bin/bash - {un} << EOF1
          CONDA_PREF=\$HOME/miniconda2
          /tmp/Miniconda2-latest-Linux-x86_64.sh -b -p \$CONDA_PREF
          rval=\$?
          if [ \$rval -ne 0 ] ; then
            echo error installing Miniconda
            exit 1
          fi
          cat << EOF2 >> \$HOME/.bashrc

          # added by Murano Bioconda package install
          export PATH=\$CONDA_PREF/bin:\\\$PATH
          EOF2
          export PATH=\$CONDA_PREF/bin:\\\$PATH
          conda config --add channels conda-forge
          conda config --add channels defaults
          conda config --add channels r
          conda config --add channels bioconda
          echo 
          EOF1
      - Try:
          - conf:Linux.runCommand($.instance.agent,format($script,un=>$._bc_user))
        Catch:
          - As: e
            Do:
              - Throw: BiocondaError
                Message: 'Bioconda install error'
      - $._environment.reporter.report($this,'Conda configured.')
      - If: $.backups != null
        Then:
          - $.backups.configure($.instance)
      # add backup config files even if backups are not enabled (in case they need to be enabled later)
      # needs to be done after database config so it can be included in backup
      - If: $._debug
        Then:
          - $._environment.reporter.report($this,'adding backup config')
      - $._duplyLib: new(DuplyBackupLib,$,instance => $.instance)
      - $stagingDir: $._duplyLib.stagingDir()
      - $exclude_bio_conda: |
          + /home/{un}
      - $._duplyLib.addExcludeConf('30-bio-conda.conf',format($exclude_bio_conda,un=>$._bc_user))
